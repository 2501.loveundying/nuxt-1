export default {
    setnavRight: (state) => {
        if (state.navRight === false) {
            return state.navRight = true;
        }
        else {
            return state.navRight = false;
        }
    },
    getSelected: (state, value) => {
        state.listItemsSelected = value;
    },
    sortSwitch: (state, value) => {
        state.sortSwitch = value;
    },
    menuSelected:(state, value)=>{
        state.menuSelected = value;
    }
}