
export default {
    navRight: state => {
        return state.navRight;
    },
    itemsHeader: state => {
        return state.headers;
    },
    listItems: state => {
        return state.listItems;
    },
    listItemsSelected: state => {
        return state.listItemsSelected;
    },
    listItemsChecked: state => {
        return state.listItemsSelected.map(x => x.id);
    },
    sortSwitch: state => {
        return state.sortSwitch;
    },
    menuLeft:state=>{
        return state.menuLeft;
    },
}