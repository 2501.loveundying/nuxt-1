export const state = () => ({
    headers: [
        {
            text: "",
            class: "font-weight-regular",
            width: "5%",
            sortable: false,
            value: "icon"
        },
        {
            text: "Name",
            class: "font-weight-regular",
            width: "40%",
            value: "name"
        },
        {
            text: "Modified",
            class: "font-weight-regular",
            width: "15%",
            value: "modified"
        },
        {
            text: "Shared",
            class: "font-weight-regular",
            width: "15%",
            value: "shared"
        },
        {
            text: "Size",
            class: "font-weight-regular",
            width: "25%",
            value: "size"
        }
    ],
    listItems: [
        {
            id: 1,
            name: "Folder One",
            dateCreate: "",
            modified: "modified",
            shared: "Private",
            size: "0,1Kb",
            sta: false,
        },
        {
            id: 2,
            name: "Folder Two",
            dateCreate: "",
            modified: "modified",
            shared: "Private",
            size: "0,1Kb",
            sta: false,
        },
        {
            id: 3,
            name: "Folder Three",
            dateCreate: "",
            modified: "modified",
            shared: "Private",
            size: "0,1Kb",
            sta: false,
        },
        {
            id: 4,
            name: "Folder Four",
            dateCreate: "",
            modified: "modified",
            shared: "Private",
            size: "0,1Kb",
            sta: false,
        },
        {
            id: 5,
            name: "Folder Five",
            dateCreate: "",
            modified: "modified",
            shared: "Private",
            size: "0,1Kb",
            sta: false,
        },
        {
            id: 6,
            name: "Folder Six",
            dateCreate: "",
            modified: "modified",
            shared: "Private",
            size: "0,1Kb",
            sta: false,
        },
    ],
    listItemsSelected:[],
    listItemsChecked:[],
    navRight: false,
    sortSwitch:'list',
    menuLeft:[
        {
            name:'Files',
            link:'/files'
        },
        {
            name:'Recent',
            link:'/recent'
        },
        {
            name:'Photos',
            link:'/photos'
        },
        {
            name:'Shared',
            link:'/shared'
        },
        {
            name:'Recyle bin',
            link:'/recylebin'
        },
        {
            name:'PCs',
            link:'/pcs'
        },
],
    
})

export default state