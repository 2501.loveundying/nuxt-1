export default {
    updateItemsSelected: ({ commit }, value) => {
        commit('getSelected', value);
    },
    updatesortSwitch: ({ commit }, value) => {
        commit('sortSwitch', value);
    },
    updatemenuSelected: ({ commit }, value) => {
        commit('menuSelected', value);
    }
}